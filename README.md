TimeSheetGenerator
---

Welcome to the front end of the timesheet generator.

1 - Setup
---

```
npm install
tsd install
Update Const URL = "<yourURL>" if needed, found in timesheet-generator.tsx file
```

2 - Compile
---
```
npm run compile
```

3 - Usage - start app after Setup and Compile
---
Start the development server with this command:
```
npm install
```

 
