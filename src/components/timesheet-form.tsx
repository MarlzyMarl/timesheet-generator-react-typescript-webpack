import * as React from 'react';
import * as DOM from 'react-dom';
import * as $ from 'jquery';
import { Button } from 'react-bootstrap';

interface props{
    timesheetInfo: {
        candidateName: string,
        clientName: string,
        jobTitle: string,
        placementEndDate: string,
        placementStartDate: string,
        placementType: string
    }
    onGenerateTimesheet: () => void
    handleChange: () => void
}

class TimesheetForm extends React.Component<props> {
    constructor(props){
        super(props);

    }

 
  render() {
    return (
      <div>
       <div className="input-group">  
           <label>Candidate Name:</label>   
            <input type="text" className="form-control" placeholder="Candidate Name" name="Candidate Name" 
                value={this.props.timesheetInfo.candidateName} onChange={this.props.handleChange.bind(this)}/>
        </div>
        <div className="input-group">   
            <label>Client Name:</label>   
            <input type="text" className="form-control" placeholder="Client Name" name="Client Name"
                value={this.props.timesheetInfo.clientName} onChange={this.props.handleChange.bind(this)}/>
        </div>
        <div className="input-group">    
            <label>Job Title:</label>  
            <input type="text" className="form-control" placeholder="Job Title" name="Job Title"
                value={this.props.timesheetInfo.jobTitle} onChange={this.props.handleChange.bind(this)}/>
        </div>
        <div className="input-group">  
          <label>Placement Start Date:</label>   
            <input type="date" className="form-control" name="Placement Start Date" placeholder="Placement End Date"
                value={this.props.timesheetInfo.placementStartDate} onChange={this.props.handleChange.bind(this)}/>
        </div>
        <div className="input-group">     
          <label>Placement End Date:</label>
            <input type="date" className="form-control" name="Placement End Date" placeholder="Placement End Date"
                value={this.props.timesheetInfo.placementEndDate} onChange={this.props.handleChange.bind(this)}/>
        </div>
        <div className="input-group">  
            <label>Placement Type:</label>   
            <select className="form-control" name="Placement Type" placeholder="Placement Type"
                value={this.props.timesheetInfo.placementType} onChange={this.props.handleChange.bind(this)}>
                <option>Weekly</option>
                <option>Monthly</option>
            </select>
        </div>
        
        <Button type="submit" className="btn btn-default" onClick={this.props.onGenerateTimesheet.bind(this)} bsSize="large">Generate Timesheet</Button>
    </div>
    );
  }
}
export default TimesheetForm;