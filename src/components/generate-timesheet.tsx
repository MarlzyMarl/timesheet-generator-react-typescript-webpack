import * as React from 'react';
import * as DOM from 'react-dom';
import * as $ from 'jquery';
import { Table } from 'react-bootstrap';

interface props{
    timesheetResponse: any
}

class GenerateTimesheet extends React.Component<props> {
    constructor(props){
        super(props);
    }

    getStandardHeaders (headers) {
        return <thead>
                <tr>
                    <th>Client Name: {this.props.timesheetResponse.clientName}</th>
                    <th>Candidate Name: {this.props.timesheetResponse.candidateName}</th>
                    <th>Job Title: {this.props.timesheetResponse.jobTitle}</th>
                </tr>
                <tr>
                    <th>Date:</th>
                    <th>Total Hours:</th>
                    <th>Rate:</th>
                </tr>
            </thead>;
    }

    createTable(tableHeaders, daysForSheet, startDate, key){
        let tableRows = [];
        for(let i = 0; i < daysForSheet; i++){
            tableRows.push(
                <tr key={3333+startDate.getDate()}>
                    <td key={startDate.getDate()}>{startDate.toISOString().substring(0, 10)}</td>
                    <td key={1111+startDate.getDate()}> </td>
                    <td key={2222+startDate.getDate()}> </td>
                </tr>);
            startDate.setDate(startDate.getDate() + 1);
        }
        return (<Table striped bordered condensed hover key={key}>
                {tableHeaders}
            <tbody>
                {tableRows}
            </tbody>
        </Table>);
    }
 
  render() {
      let date = new Date(this.props.timesheetResponse.placementStartDate);
      let tableHeaders = this.getStandardHeaders(this.props.timesheetResponse);
      var tables = [];
      let key = 1000;

      this.props.timesheetResponse.days.map((daysForSheet) => {
        tables.push(this.createTable(tableHeaders, daysForSheet, date, key));
        key++;
      });
      
    return (
      <div>
        {tables}
    </div>
    );
  }
}
export default GenerateTimesheet;