console.log('applications started');
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import TimesheetGenerator from './timesheet-generator';
 
document.addEventListener('DOMContentLoaded', function() {
  ReactDOM.render(
    React.createElement(TimesheetGenerator),
    document.getElementById('mount')
  );
});