import * as React from 'react';
import * as DOM from 'react-dom';
import * as $ from 'jquery';
import TimesheetForm from './components/timesheet-form';
import GenerateTimesheet from './components/generate-timesheet';

const URL = "http://localhost:18435/api/timesheet";

interface GenerateTimesheetState {
    timesheetInfo: {
        candidateName: string,
        clientName: string,
        jobTitle: string,
        placementEndDate: string,
        placementStartDate: string,
        placementType: string
    },
    timesheetResponse: any
}
var errorArray = [];
var showErrors = false;
var response = {};

class TimesheetGenerator extends React.Component<any, GenerateTimesheetState> {
    constructor(){
        super();
        this.state = {
            timesheetInfo: {                
                candidateName: "",
                clientName: "",
                jobTitle: "",
                placementEndDate: "",
                placementStartDate: "",
                placementType: 'Weekly'
            },
            timesheetResponse: undefined
        };
    }

    handleChange (event){
        let timesheetInfo = this.state.timesheetInfo;
        switch (event.target.name){
            case 'Candidate Name': 
                timesheetInfo.candidateName = event.target.value;
                this.setState({timesheetInfo: timesheetInfo});
                break;
            case 'Client Name': 
                timesheetInfo.clientName = event.target.value;
                this.setState({timesheetInfo: timesheetInfo});
                break;
            case 'Job Title': 
                timesheetInfo.jobTitle = event.target.value;
                this.setState({timesheetInfo: timesheetInfo});
                break;
            case 'Placement Start Date': 
                timesheetInfo.placementStartDate = event.target.value;
                this.setState({timesheetInfo: timesheetInfo});
                break;
            case 'Placement End Date': 
                timesheetInfo.placementEndDate = event.target.value;
                this.setState({timesheetInfo: timesheetInfo});
                break;
            case 'Placement Type': 
                timesheetInfo.placementType = event.target.value;
                this.setState({timesheetInfo: timesheetInfo});
                break;
                
        }
    } 

    validatePostMessage(payload) {
        errorArray = [];
        let isValid = true;
        for (var key in payload) {
            if (!payload[key]) {
                console.log(key + " -> " + payload[key]);
                errorArray.push(key);
                isValid = false;
            }
        }
        return isValid;
    }

    onGenerateTimesheet (){
        let timesheetInfo = {
            candidateName: this.state.timesheetInfo.candidateName,
            clientName: this.state.timesheetInfo.clientName,
            jobTitle: this.state.timesheetInfo.jobTitle,
            placementEndDate: this.state.timesheetInfo.placementEndDate,
            placementStartDate: this.state.timesheetInfo.placementStartDate,
            placementType: this.state.timesheetInfo.placementType
        }
        if(!this.validatePostMessage(timesheetInfo)) {
            return false;
        }

        $.ajax({
            url: URL,
            method: 'POST',
            dataType: 'JSON',
            data: JSON.stringify(timesheetInfo),
            contentType: "application/json; charset=utf-8",
            success: (data) => {
                console.log(data);
                this.setState({timesheetResponse: data});
              //arrow function is needed otherwise this is no in scope... if no arrow function then i need to use .bind(this) at the end of the function...
            },
        });
    }
 
  render() {
      let renderTimesheets = <div></div>;
      if(this.state.timesheetResponse !== undefined){
          renderTimesheets = <GenerateTimesheet timesheetResponse={this.state.timesheetResponse}/>
      }
    return (
        <div className="container">
            <h1>Timesheet Generator</h1>
            <TimesheetForm handleChange={this.handleChange.bind(this)} onGenerateTimesheet={this.onGenerateTimesheet.bind(this)} timesheetInfo={this.state.timesheetInfo}/>
            {renderTimesheets}
        </div>
    );
  }
}
export default TimesheetGenerator;