const path = require('path');
var webpack = require('webpack');
 
module.exports = {
  context: path.join(__dirname, 'src'),
  entry: [
    './main.ts',
  ],
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader', 
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
  },
};
